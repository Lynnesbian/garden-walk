# LINUX
set -e
echo "Building for Linux..."
mkdir build/linux || true
cross build --target x86_64-unknown-linux-gnu --release
cp target/x86_64-unknown-linux-gnu/release/garden-walk build/linux
strip build/linux/garden-walk
cp -r res build/linux
tar caf garden-walk-linux.tar.xz --directory ./build/linux .

# WINDOWS
echo "Building for Windows..."
mkdir build/win || true
cross build --target x86_64-pc-windows-gnu --release
# strip
strip target/x86_64-pc-windows-gnu/release/garden-walk.exe
# zip
cp target/x86_64-pc-windows-gnu/release/garden-walk.exe build/win
cp -r res build/win
cd build/win
zip -r -9 ../../garden-walk-windows.zip garden-walk.exe res ./*.dll || cd -
cd -
