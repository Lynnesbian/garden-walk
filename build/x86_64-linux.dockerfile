FROM ghcr.io/cross-rs/x86_64-unknown-linux-gnu:latest

RUN apt-get update && \
    apt-get install --assume-yes pkg-config libx11-dev libxi-dev libgl1-mesa-dev libasound2-dev
