Garden Walk
===
<div align="center">
![Screenshot](/doc/screenshot.png "Screenshot")
</div>

## Controls
- `WASD` or arrow keys to move
- `P` or `Escape` to pause
- `Q` to quit

## Building
You'll need a few dependencies, which are given below for some popular distros. Successfully cloning the repository 
requires Git LFS.
### Fedora/Red Hat
`sudo dnf install pkg-config libX11-devel libXi-devel mesa-libGL-devel alsa-lib-devel git-lfs`
### Debian/Ubuntu
`sudo apt install pkg-config libx11-dev libxi-dev libgl1-mesa-dev libasound2-dev git-lfs`
### Arch Linux
`sudo pacman -S pkg-config libx11 libxi mesa-libgl alsa-lib git-lfs`

## Licensing
Source code is licensed as GPL3.

Image and audio resources (i.e. all files contained within the `res/` subdirectory, as well as the screenshot in the 
`doc/` subdirectory) are licensed as CC-BY-SA-NC.
