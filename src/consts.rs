// window resolution
pub const DISPLAY_WIDTH: u32 = 800;
pub const DISPLAY_HEIGHT: u32 = 600;

// texture size
pub const TEXTURE_WIDTH: u32 = 64;
pub const TEXTURE_HEIGHT: u32 = 64;

// number of textures
pub const FLOOR_TEXTURE_MAX: usize = 6;
pub const WALL_TEXTURE_MAX: usize = 6;
pub const SPRITE_TEXTURE_MAX: usize = 2;
