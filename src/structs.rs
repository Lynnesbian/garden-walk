use vector2d::Vector2D;

#[derive(Clone)]
pub struct Config {
	pub threads: usize,
	pub map_size: MapSize,
	pub render_mode: RenderMode,
}

impl Default for Config {
	fn default() -> Self {
		Self {
			threads: 4,
			map_size: MapSize::Medium,
			render_mode: RenderMode::Quality,
		}
	}
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, strum::Display, strum::EnumIter, strum::EnumString)]
pub enum MapSize {
	Small,
	Medium,
	Large,
	Gigantic,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, strum::Display, strum::EnumIter, strum::EnumString)]
pub enum RenderMode {
	Quality,
	Performance,
}

impl MapSize {
	pub fn dimensions(&self) -> (usize, usize) {
		match self {
			MapSize::Small => (24, 24),
			MapSize::Medium => (36, 36),
			MapSize::Large => (48, 48),
			MapSize::Gigantic => (64, 64),
		}
	}
}

pub struct Dimensions {
	pub map_width: usize,
	pub map_height: usize,
	pub frame_width: u32,
	pub frame_height: u32,
}

pub struct Point {
	pub x: f64,
	pub y: f64,
}

impl Point {
	pub fn new(x: f64, y: f64) -> Self {
		Self { x, y }
	}
}

pub struct State {
	pub pos: Point,
	pub dir: Vector2D<f64>,
	pub velocity: f64,
	pub plane: Vector2D<f64>,
}

pub struct Sprite {
	pub pos: Point,
	pub index: u8,
}

pub enum GameState {
	Start,
	Play,
	Pause,
}
