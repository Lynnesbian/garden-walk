use std::io::BufRead;
use std::path::{Path, PathBuf};
use std::time::Instant;

use ::rand::prelude::*;
use anyhow::anyhow;
use egui_macroquad::egui;
use egui_macroquad::egui::style::Spacing;
use egui_macroquad::egui::Style;
use futures::future;
use image::io::Reader as ImageReader;
use image::{Rgba, RgbaImage};
use macroquad::audio;
use macroquad::audio::PlaySoundParams;
use macroquad::math::f64;
use macroquad::prelude::*;
use rayon::prelude::*;
use strum::IntoEnumIterator;
use vector2d::Vector2D; // TODO: replace with Vec2?

use crate::consts::*;
use crate::structs::{Config, Dimensions, GameState, MapSize, Point, RenderMode, Sprite, State};

pub async fn run() -> anyhow::Result<()> {
	// start loading music right away, because it takes a while
	let title_music = audio::load_sound("res/bgm/Garden_Walk.ogg");
	let game_music = audio::load_sound("res/bgm/Flower_Waltz.ogg");

	// get user config
	let mut config = Config {
		threads: usize::max(1, num_cpus::get() / 2),
		..Default::default()
	};

	// check for missing files
	let missing_files: Vec<_> = include_bytes!("../resources.txt")
		.lines()
		.skip(1)
		.filter_map(|e| e.ok())
		.filter(|e| !e.is_empty() && !Path::exists(&PathBuf::from("res").join(PathBuf::from(e))))
		.collect();

	// configure ui
	egui_macroquad::cfg(|ctx| {
		// make gui more readable
		ctx.set_pixels_per_point(1.5);
		ctx.set_style(Style {
			spacing: Spacing {
				item_spacing: (5.0, 10.0).into(),
				..Default::default()
			},
			..Default::default()
		});
		// light theme
		ctx.set_visuals(egui::Visuals::light());
	});

	if !missing_files.is_empty() {
		loop {
			// warn the user about the missing files, refuse to run
			egui_macroquad::ui(|ctx| {
				egui::CentralPanel::default().show(ctx, |ui| {
					ui.heading("Error");
					ui.label("The following required resource files are missing:");
					ui.add_space(10.0);

					egui::ScrollArea::vertical().max_height(150.0).show(ui, |ui| {
						for missing_file in &missing_files {
							ui.label(format!("\t- res/{}", missing_file));
						}
					});

					ui.add_space(10.0);
					ui.label("The game cannot run.");
				});
			});

			egui_macroquad::draw();
			next_frame().await;
		}
	}

	let mut done = false;

	while !done {
		egui_macroquad::ui(|ctx| {
			if ctx.input(|i| i.key_pressed(egui::Key::Enter)) {
				done = true;
			}

			egui::CentralPanel::default().show(ctx, |ui| {
				ui.with_layout(egui::Layout::top_down_justified(egui::Align::Center), |ui| {
					ui.heading("Garden Walk - Settings");
				});

				ui.add_space(10.0);

				ui.horizontal(|ui| {
					ui.label("Threads");
					ui.add(egui::Slider::new(&mut config.threads, 1..=num_cpus::get()));
				});

				ui.horizontal(|ui| {
					ui.label("Map size");
					egui::ComboBox::from_id_source("map_size")
						.selected_text(config.map_size.to_string())
						.show_ui(ui, |ui| {
							for size in MapSize::iter() {
								ui.selectable_value(&mut config.map_size, size, size.to_string());
							}
						})
				});

				ui.horizontal(|ui| {
					ui.label("Rendering mode");
					egui::ComboBox::from_id_source("render_mode")
						.selected_text(config.render_mode.to_string())
						.show_ui(ui, |ui| {
							for mode in RenderMode::iter() {
								ui.selectable_value(&mut config.render_mode, mode, mode.to_string());
							}
						})
				});

				ui.add_space(10.0);
				if ui.button("\tGo!\t").clicked() {
					done = true;
				}

				ui.add_space(10.0);
				ui.with_layout(egui::Layout::top_down_justified(egui::Align::Center), |ui| {
					ui.style_mut().override_text_style = Some(egui::TextStyle::Small);
					ui.label("©2023 Lynnear Software");
				});
			});
		});
		egui_macroquad::draw();
		next_frame().await;
	}

	// initialise variables
	let start_time = Instant::now();
	let mut time = Instant::now();
	let mut prev_time: Instant;
	let mut framerate = vec![0i32; 15];

	let mut state = State {
		pos: Point::new(22.0, 12.0),
		dir: Vector2D::new(-1.0, 0.0),
		velocity: 0.0,
		plane: Vector2D::new(0.0, 0.6),
	};
	let mut game_state = GameState::Start;
	let fullscreen_texture_params = DrawTextureParams {
		dest_size: Some(Vec2::new(DISPLAY_WIDTH as f32, DISPLAY_HEIGHT as f32)),
		..Default::default()
	};

	let dim = {
		let (map_width, map_height) = config.map_size.dimensions();
		let (frame_width, frame_height) = if config.render_mode == RenderMode::Quality {
			(DISPLAY_WIDTH, DISPLAY_HEIGHT)
		} else {
			(DISPLAY_WIDTH / 2, DISPLAY_HEIGHT / 2)
		};
		Dimensions {
			map_width,
			map_height,
			frame_width,
			frame_height,
		}
	};

	// define frame chunks
	let mut frame_chunks = vec![(0u32, 0u32); config.threads];
	{
		let chunk_step = dim.frame_width as f64 / config.threads as f64;
		for (x, chunk) in frame_chunks.iter_mut().enumerate().take(config.threads) {
			*chunk = ((chunk_step * x as f64) as u32, (chunk_step * (x as f64 + 1.0)) as u32);
		}
	}

	draw_text_centred("NOW LOADING", 32, WHITE);
	next_frame().await;

	// load textures
	let wall_textures = load_textures("wall", WALL_TEXTURE_MAX)?;
	let floor_textures = load_textures("floor", FLOOR_TEXTURE_MAX)?;
	let sprite_textures = load_textures("sprites", SPRITE_TEXTURE_MAX)?;

	// splash screen textures
	// TODO: handle missing splash and pause textures, and music
	let splash_textures = vec![
		load_texture("res/img/ui/splash.png"),
		load_texture("res/img/ui/logos.png"),
		load_texture("res/img/ui/start.png"),
	];

	// pause screen
	let pause_texture = load_texture("res/img/ui/pause.png");
	let pause_background = Texture2D::from_rgba8(
		DISPLAY_WIDTH as u16,
		DISPLAY_HEIGHT as u16,
		vec![0u8; (4 * DISPLAY_HEIGHT * DISPLAY_WIDTH) as usize].as_slice(),
	);

	// generate map
	draw_text_centred("GENERATING MAP", 32, WHITE);
	next_frame().await;

	let map = generate_map(dim.map_width, dim.map_height);

	// generate random floor texture map
	let mut floor_map: Vec<Vec<u8>> = vec![];
	for x in 0..dim.map_width {
		floor_map.push(vec![]);
		for _ in 0..dim.map_height {
			floor_map[x].push(thread_rng().gen_range(0..=FLOOR_TEXTURE_MAX) as u8);
		}
	}

	// generate sprite positions
	let mut sprites = Vec::with_capacity(50);
	for _ in 0..50 {
		sprites.push(Sprite {
			pos: Point::new(
				thread_rng().gen_range(1.0..(dim.map_width - 1) as f64),
				thread_rng().gen_range(1.0..(dim.map_height - 1) as f64),
			),
			index: thread_rng().gen_range(0..=SPRITE_TEXTURE_MAX) as u8,
		});
	}

	// set up thread pool
	rayon::ThreadPoolBuilder::new()
		.num_threads(config.threads)
		.build_global()?;

	// framebuffers
	let mut background = Image::gen_image_color(dim.frame_width as u16, dim.frame_height as u16, BLACK);
	let mut foreground = Image::gen_image_color(dim.frame_width as u16, dim.frame_height as u16, BLACK);

	// allocate macroquad textures
	draw_text_centred("LOADING RESOURCES", 32, WHITE);
	next_frame().await;

	let empty = vec![0u8; (4 * dim.frame_width * dim.frame_height) as usize];
	let fg_texture = Texture2D::from_rgba8(dim.frame_width as u16, dim.frame_height as u16, empty.as_slice());
	fg_texture.set_filter(FilterMode::Nearest);
	let bg_texture = Texture2D::from_rgba8(dim.frame_width as u16, dim.frame_height as u16, empty.as_slice());
	bg_texture.set_filter(FilterMode::Nearest);

	let mut chunk_textures = Vec::with_capacity(frame_chunks.len());
	for chunk in &frame_chunks {
		let chunk_width = chunk.0.abs_diff(chunk.1);
		chunk_textures.push(Texture2D::from_rgba8(
			chunk_width as u16,
			dim.frame_height as u16,
			vec![0u8; (4 * chunk_width * dim.frame_height as u32) as usize].as_slice(),
		))
	}
	for tex in &chunk_textures {
		tex.set_filter(FilterMode::Nearest);
	}

	// pause state
	let mut paused = false;

	// wait for things to finish loading
	let title_music = title_music.await?;
	let game_music = game_music.await?;
	let pause_texture = pause_texture.await?;
	pause_texture.set_filter(FilterMode::Nearest);

	let mut splash_textures: Vec<_> = future::join_all(splash_textures)
		.await
		.into_iter()
		.map(|t| t.unwrap())
		.map(|t| {
			t.set_filter(FilterMode::Nearest);
			t
		})
		.collect();

	// start the title music
	audio::play_sound_once(title_music);

	// return Ok(());

	'game: loop {
		// get frame time
		prev_time = time;
		time = Instant::now();
		let frame_time = time.duration_since(prev_time);
		framerate.insert(0, get_fps());
		framerate.pop();

		// handle quitting, regardless of game state
		if is_key_down(KeyCode::Q) || is_quit_requested() {
			break 'game;
		}

		// run game!
		match game_state {
			GameState::Start => {
				// change texture every 2 seconds
				let tex = splash_textures[usize::min(
					splash_textures.len() - 1,
					(time.duration_since(start_time).as_secs() / 2) as usize,
				)];
				draw_texture_ex(tex, 0.0, 0.0, WHITE, fullscreen_texture_params.clone());

				// wait for enter keypress
				if is_key_pressed(KeyCode::Enter) {
					// switch state
					game_state = GameState::Play;
					// switch music track
					audio::stop_sound(title_music);
					audio::play_sound(
						game_music,
						PlaySoundParams {
							looped: true,
							volume: 1.0,
						},
					);
					// free up a smidgen of VRAM
					splash_textures.clear();
				}
			}
			GameState::Play => {
				// handle unpausing
				if paused {
					paused = false;
					audio::set_sound_volume(game_music, 1.0);
				}

				// set speed based on time delta
				let move_speed = frame_time.as_secs_f64() * 0.25;
				let rotate_speed = frame_time.as_secs_f64() * 3.0;

				// handle keys
				if is_key_down(KeyCode::W) || is_key_down(KeyCode::Up) {
					// accelerate forward
					state.velocity += move_speed * 2.0;
				}
				if is_key_down(KeyCode::S) || is_key_down(KeyCode::Down) {
					// accelerate backward
					state.velocity -= move_speed * 2.0;
				}
				if is_key_down(KeyCode::A)
					|| is_key_down(KeyCode::Left)
					|| is_key_down(KeyCode::D)
					|| is_key_down(KeyCode::Right)
				{
					// left or right?
					let m = if is_key_down(KeyCode::A) || is_key_down(KeyCode::Left) { 1.0 } else { -1.0 };
					// rotate camera direction
					let old_dir_x = state.dir.x;
					state.dir.x = state.dir.x * f64::cos(m * rotate_speed) - state.dir.y * f64::sin(m * rotate_speed);
					state.dir.y = old_dir_x * f64::sin(m * rotate_speed) + state.dir.y * f64::cos(m * rotate_speed);

					// rotate camera plane
					let old_plane_x = state.plane.x;
					state.plane.x = state.plane.x * f64::cos(m * rotate_speed) - state.plane.y * f64::sin(m * rotate_speed);
					state.plane.y = old_plane_x * f64::sin(m * rotate_speed) + state.plane.y * f64::cos(m * rotate_speed);
				}

				if state.velocity.abs() <= move_speed * 1.5 {
					state.velocity = 0.0; // slow to a stop
				} else if state.velocity.is_sign_positive() {
					state.velocity -= move_speed; // apply deceleration...
				} else {
					state.velocity += move_speed; // ...for both directions
				}

				// cap velocity
				state.velocity = state.velocity.clamp(-move_speed * 30.0, move_speed * 30.0);

				// handle movement
				if map[(state.pos.x + state.dir.x * state.velocity) as usize][state.pos.y as usize] == 0 {
					state.pos.x += state.dir.x * state.velocity
				};
				if map[state.pos.x as usize][(state.pos.y + state.dir.y * state.velocity) as usize] == 0 {
					state.pos.y += state.dir.y * state.velocity
				};

				// render!
				// render floor and ceiling
				render_floor_ceiling(&mut background, &state, &floor_textures, &floor_map, &dim);

				// render walls
				let (z_buf, rendered_chunks): (Vec<_>, Vec<_>) = frame_chunks
					.par_iter()
					.map(|&c| render_chunk(c, &state, &wall_textures, &map, &dim))
					.collect();
				let z_buf: Vec<_> = z_buf.into_iter().flatten().collect();

				// render sprites
				// clear foreground buffer
				for chunk in foreground.get_image_data_mut().iter_mut() {
					chunk[0] = 0;
					chunk[1] = 0;
					chunk[2] = 0;
					chunk[3] = 0;
				}
				render_sprites(&mut foreground, &state, &z_buf, &sprites, &sprite_textures, &dim);

				// draw to canvas
				// first, draw the background texture...
				bg_texture.update(&background);
				draw_texture_ex(bg_texture, 0.0, 0.0, WHITE, fullscreen_texture_params.clone());

				// ...and now the walls...
				for (index, (frame_buf, chunk)) in rendered_chunks.iter().zip(frame_chunks.iter()).enumerate() {
					let chunk_width = chunk.0.abs_diff(chunk.1);
					let mut draw_x = chunk.0 as f32;
					let mut draw_params = DrawTextureParams {
						dest_size: Some(Vec2::new(chunk_width as f32, DISPLAY_HEIGHT as f32 + 1.0)),
						..Default::default()
					};

					// handle performance mode
					if config.render_mode == RenderMode::Performance {
						draw_x *= 2.0;
						draw_params.dest_size = Some(Vec2::new(chunk_width as f32 * 2.0, DISPLAY_HEIGHT as f32 + 2.0));
					}

					chunk_textures[index].update(frame_buf);
					draw_texture_ex(chunk_textures[index], draw_x, 0.0, WHITE, draw_params);
				}

				// ...and finally, the sprites.
				fg_texture.update(&foreground);
				draw_texture_ex(fg_texture, 0.0, 0.0, WHITE, fullscreen_texture_params.clone());

				// then, check if the game is paused
				if is_key_pressed(KeyCode::Escape) || is_key_pressed(KeyCode::P) {
					// take a screenshot to use for the pause background
					pause_background.update(&get_screen_data());
					game_state = GameState::Pause;
				}

				// and then draw the FPS!
				let fps = (framerate.iter().sum::<i32>() as usize) / framerate.len();
				draw_text(fps.to_string().as_str(), 10.0, 20.0, 24.0, BLACK);
			}
			GameState::Pause => {
				if !paused {
					paused = true;
					// turn down the music
					audio::set_sound_volume(game_music, 0.33);
				}

				// for some reason, the pause background gets drawn upside down unless we do this
				let params = DrawTextureParams {
					flip_y: true,
					..Default::default()
				};
				draw_texture_ex(pause_background, 0.0, 0.0, WHITE, params);

				// add the pause texture
				draw_texture_ex(pause_texture, 0.0, 0.0, WHITE, fullscreen_texture_params.clone());

				if is_key_pressed(KeyCode::Escape) || is_key_pressed(KeyCode::P) {
					game_state = GameState::Play;
				}
			}
		}

		next_frame().await
	}
	Ok(())
}

fn load_textures(subdirectory: &str, count: usize) -> anyhow::Result<Vec<RgbaImage>> {
	let mut textures = Vec::with_capacity(count + 1);
	for i in 0..=count {
		let path = format!("res/img/{}/{}.png", subdirectory, i);
		if !Path::exists(path.as_ref()) {
			// shouldn't happen, since we check at startup, but you never know i suppose
			return Err(anyhow!("Missing file: {}", path));
		}
		textures.push(ImageReader::open(path)?.decode()?.to_rgba8());
	}
	Ok(textures)
}

fn generate_map(map_width: usize, map_height: usize) -> Vec<Vec<u8>> {
	let mut map = vec![vec![0u8; map_width]; map_height];

	// place some random bushes
	for _ in 0..10 {
		map[thread_rng().gen_range(1..map_width)][thread_rng().gen_range(1..map_height)] = 2u8;
	}

	// place some other blocks
	for _ in 0..10 {
		map[thread_rng().gen_range(1..map_width)][thread_rng().gen_range(1..map_height)] =
			thread_rng().gen_range(2..=WALL_TEXTURE_MAX + 1) as u8;
	}

	// surround the map with walls
	map[0] = vec![1u8; map_width];
	map[map_height - 1] = vec![1u8; map_width];
	for row in map.iter_mut() {
		row[0] = 1;
		row[map_width - 1] = 1;
	}

	// build some random rooms
	for _ in 0..3 {
		let room_height = thread_rng().gen_range(4..8);
		let room_width = thread_rng().gen_range(4..8);
		let room_x = thread_rng().gen_range(1..map_width - room_width - 1);
		let room_y = thread_rng().gen_range(1..map_height - room_height - 1);
		let room_type = thread_rng().gen_range(2..=WALL_TEXTURE_MAX) as u8;
		let (entrance_x, entrance_y) = {
			if thread_rng().gen() {
				// horizontal door
				if thread_rng().gen() {
					(0, room_height / 2) // on left
				} else {
					(room_width - 1, room_height / 2) // on right
				}
			} else {
				// vertical door
				if thread_rng().gen() {
					(room_width / 2, 0) // on top
				} else {
					(room_width / 2, room_height - 1) // on bottom
				}
			}
		};

		// place room
		for x in 0..room_width {
			for y in 0..room_height {
				if y == 0 || y == room_height - 1 || x == 0 || x == room_width - 1 {
					// place wall
					map[x + room_x][y + room_y] = room_type;
				}
				if x == entrance_x && y == entrance_y {
					// place entrance
					map[x + room_x][y + room_y] = 0
				}
			}
		}
	}

	map
}

fn render_floor_ceiling(
	background: &mut Image,
	state: &State,
	floor_textures: &[RgbaImage],
	floor_map: &[Vec<u8>],
	dim: &Dimensions,
) {
	let camera_z = dim.frame_height as f64 / 2.0;
	// ray direction for leftmost and rightmost rays
	let ray_x_0 = state.dir.x - state.plane.x;
	let ray_y_0 = state.dir.y - state.plane.y;
	let ray_x_1 = state.dir.x + state.plane.x;
	let ray_y_1 = state.dir.y + state.plane.y;

	for y in 0..dim.frame_height {
		// distance from vertical centre of the screen
		let p = y as i32 - (dim.frame_height as i32 >> 1);

		// horizontal distance from the camera to the floor for the current row
		let row_dist = camera_z / p as f64;

		// absolute step vector we have to add for each x (parallel to camera plane)
		let step_x = row_dist * (ray_x_1 - ray_x_0) / dim.frame_width as f64;
		let step_y = row_dist * (ray_y_1 - ray_y_0) / dim.frame_width as f64;

		// absolute coordinates for leftmost column
		let mut floor_x = state.pos.x + row_dist * ray_x_0;
		let mut floor_y = state.pos.y + row_dist * ray_y_0;

		for x in 0..dim.frame_width {
			// texture coordinate
			let tex_x = (TEXTURE_WIDTH as f64 * floor_x.fract()) as u32;
			let tex_y = (TEXTURE_HEIGHT as f64 * floor_y.fract()) as u32;

			floor_x += step_x;
			floor_y += step_y;

			// draw the floor and ceiling
			// floor
			let colour =
				if floor_x >= dim.map_width as f64 || floor_y >= dim.map_height as f64 || floor_x < 0.0 || floor_y < 0.0 {
					// out of map range, fall back to green
					&Rgba([89u8, 153, 98, 255])
				} else {
					let floor_texture = &floor_textures[floor_map[floor_x as usize][floor_y as usize] as usize];
					floor_texture.get_pixel(tex_x, tex_y)
				};

			let index = (x + (y as u32 * dim.frame_width)) as usize;
			let pixel = &mut background.get_image_data_mut()[index];
			pixel[0] = colour[0];
			pixel[1] = colour[1];
			pixel[2] = colour[2];
			pixel[3] = colour[3]; // alpha

			// ceiling
			// let colour = sky.get_pixel(tex_x, tex_y);
			let colour = Rgba([87, 165, 173, 255]);
			let index = (x + ((dim.frame_height - y as u32 - 1) * dim.frame_width)) as usize;
			let pixel = &mut background.get_image_data_mut()[index];
			pixel[0] = colour[0];
			pixel[1] = colour[1];
			pixel[2] = colour[2];
			pixel[3] = colour[3]; // alpha
		}
	}
}

fn render_chunk(
	chunk: (u32, u32),
	state: &State,
	textures: &[RgbaImage],
	map: &[Vec<u8>],
	dim: &Dimensions,
) -> (Vec<f64>, Image) {
	let chunk_width = chunk.0.abs_diff(chunk.1);
	let mut z_buf = vec![0.0; chunk_width as usize];
	let mut frame_buf =
		Image::gen_image_color(chunk_width as u16, dim.frame_height as u16, Color::new(0.0, 0.0, 0.0, 0.0));

	for x in chunk.0..chunk.1 {
		// calculate ray position and direction
		let camera_x = 2.0 * (x as f64) / (dim.frame_width as f64) - 1.0;
		let ray_x = state.dir.x + state.plane.x * camera_x;
		let ray_y = state.dir.y + state.plane.y * camera_x;

		// which grid cell the player is in
		let mut map_x = state.pos.x as i32;
		let mut map_y = state.pos.y as i32;

		// length of ray from one intersection to the next
		let delta_dist_x = if ray_x == 0.0 { 1e30 } else { f64::abs(1.0 / ray_x) };
		let delta_dist_y = if ray_y == 0.0 { 1e30 } else { f64::abs(1.0 / ray_y) };

		// did we hit a north/south wall or an east/west wall?
		let mut north_south: bool;

		// calculate initial x step direction and distance
		let (step_x, mut side_dist_x) = if ray_x < 0.0 {
			(-1, state.pos.x.fract() * delta_dist_x)
		} else {
			(1, (map_x as f64 + 1.0 - state.pos.x) * delta_dist_x)
		};

		// calculate initial y step direction and distance
		let (step_y, mut side_dist_y) = if ray_y < 0.0 {
			(-1, state.pos.y.fract() * delta_dist_y)
		} else {
			(1, (map_y as f64 + 1.0 - state.pos.y) * delta_dist_y)
		};

		// DDA (digital differential analysis) start!
		loop {
			// jump to next map square
			if side_dist_x < side_dist_y {
				side_dist_x += delta_dist_x;
				map_x += step_x;
				north_south = false;
			} else {
				side_dist_y += delta_dist_y;
				map_y += step_y;
				north_south = true;
			}

			if map[map_x as usize][map_y as usize] > 0 {
				break;
			}
		}

		// distance from camera plane to wall
		let wall_dist = if north_south { side_dist_y - delta_dist_y } else { side_dist_x - delta_dist_x };

		// line height to draw on the screen
		let line_height = (dim.frame_height as f64 / wall_dist) as i32;

		// calculate line start and end points
		let draw_start = i32::max(0, -line_height / 2 + dim.frame_height as i32 / 2);
		let draw_end = i32::min(dim.frame_height as i32 - 1, line_height / 2 + dim.frame_height as i32 / 2);

		// get texture
		let texture = &textures[map[map_x as usize][map_y as usize] as usize - 1];

		// where did the ray hit the wall?
		let wall_x = if north_south { state.pos.x + wall_dist * ray_x } else { state.pos.y + wall_dist * ray_y }.fract();

		// find x coordinate on the texture
		let mut tex_x = (wall_x * TEXTURE_WIDTH as f64) as usize;
		if (north_south && ray_y > 0.0) || (!north_south && ray_x < 0.0) {
			tex_x = TEXTURE_WIDTH as usize - tex_x - 1;
		}

		// how much further to move through the texture per pixel
		let step = TEXTURE_HEIGHT as f64 / line_height as f64;
		// starting coordinate
		let mut tex_pos = (draw_start - dim.frame_height as i32 / 2 + line_height / 2) as f64 * step;

		for y in draw_start..draw_end {
			// cast texture coordinate to integer, mask in case of overflow
			let tex_y = tex_pos as u32 & (TEXTURE_HEIGHT - 1);
			tex_pos += step;
			let mut colour = *texture.get_pixel(tex_x as u32, tex_y);

			// darken north/south aligned walls by halving colour values to provide illusion of depth
			if north_south {
				colour[0] >>= 1;
				colour[1] >>= 1;
				colour[2] >>= 1;
			}

			// draw the pixel!
			let index = (x - chunk.0 + (y as u32 * chunk_width)) as usize;
			let pixel = &mut frame_buf.get_image_data_mut()[index];
			pixel[0] = colour[0];
			pixel[1] = colour[1];
			pixel[2] = colour[2];
			pixel[3] = colour[3]; // alpha
		}

		// set z buffer for sprite casting
		z_buf[(x - chunk.0) as usize] = wall_dist;
	}
	(z_buf, frame_buf)
}

fn render_sprites(
	foreground: &mut Image,
	state: &State,
	z_buf: &[f64],
	sprites: &[Sprite],
	sprite_textures: &[RgbaImage],
	dim: &Dimensions,
) {
	// sort sprites
	let mut sprites: Vec<_> = sprites
		.iter()
		.map(|s|
			// calculate distance from player
			(s, (state.pos.x - s.pos.x) * (state.pos.x - s.pos.x) + (state.pos.y - s.pos.y) * (state.pos.y - s.pos.y)))
		.collect();
	sprites.sort_by(|a, b| b.1.partial_cmp(&a.1).unwrap());

	for (sprite, _) in sprites {
		let sprite_x = sprite.pos.x - state.pos.x;
		let sprite_y = sprite.pos.y - state.pos.y;
		let inv_det = 1.0 / (state.plane.x * state.dir.y - state.dir.x * state.plane.y);
		let transform_x = inv_det * (state.dir.y * sprite_x - state.dir.x * sprite_y);
		let transform_y = inv_det * (-state.plane.y * sprite_x + state.plane.x * sprite_y);
		let sprite_screen_x = ((dim.frame_width / 2) as f64 * (1.0 + transform_x / transform_y)) as i32;

		// calculate height of the sprite
		let sprite_height = i32::abs((dim.frame_height as f64 / transform_y) as i32);
		let draw_start_y = i32::max(0, -sprite_height / 2 + dim.frame_height as i32 / 2);
		let draw_end_y = i32::min(dim.frame_height as i32 - 1, sprite_height / 2 + dim.frame_height as i32 / 2);

		// calculate width of the sprite
		let sprite_width = sprite_height; // sprites are square!
		let draw_start_x = i32::max(0, -sprite_width / 2 + sprite_screen_x);
		let draw_end_x = i32::min(dim.frame_width as i32 - 1, sprite_width / 2 + sprite_screen_x);

		for stripe in draw_start_x..draw_end_x {
			let tex_x =
				((256 * (stripe - (-sprite_width / 2 + sprite_screen_x)) * TEXTURE_WIDTH as i32 / sprite_width) / 256) as u32;
			// only continue if...
			// - the sprite is in front of the camera
			// - it's not off the edge of the screen
			// - it's not behind a wall (z buffer)
			if transform_y > 0.0 && stripe < dim.frame_width as i32 && transform_y < z_buf[stripe as usize] {
				for y in draw_start_y..draw_end_y {
					let d = y * 256 - dim.frame_height as i32 * 128 + sprite_height * 128;
					let tex_y = (((d * TEXTURE_HEIGHT as i32) / sprite_height) / 256) as u32;
					if tex_x > TEXTURE_WIDTH || tex_y > TEXTURE_HEIGHT {
						continue;
					}
					let colour = sprite_textures[sprite.index as usize].get_pixel(tex_x, tex_y);
					if colour[3] == 0 {
						// don't draw transparent pixels - they could overwrite pre-existing sprites!
						continue;
					}
					let index = (stripe + (y * dim.frame_width as i32)) as usize;
					let pixel = &mut foreground.get_image_data_mut()[index];
					pixel[0] = colour[0];
					pixel[1] = colour[1];
					pixel[2] = colour[2];
					pixel[3] = colour[3];
				}
			}
		}
	}
}

fn draw_text_centred(text: &str, font_size: u16, colour: Color) {
	// get text dimensions
	let dimensions = measure_text(text, None, font_size, 1.0);
	// draw text in centre of screen
	draw_text(
		text,
		(DISPLAY_WIDTH / 2) as f32 - dimensions.width / 2.0,
		(DISPLAY_HEIGHT / 2) as f32 - dimensions.height / 2.0,
		font_size as f32,
		colour
	)
}
