use crate::consts::{DISPLAY_HEIGHT, DISPLAY_WIDTH};

mod consts;
mod game;
mod structs;

fn window_conf() -> macroquad::window::Conf {
	macroquad::window::Conf {
		window_title: format!("Garden Walk {}", env!("CARGO_PKG_VERSION")),
		window_resizable: false,
		window_height: DISPLAY_HEIGHT as i32,
		window_width: DISPLAY_WIDTH as i32,
		..Default::default()
	}
}

#[macroquad::main(window_conf)]
async fn main() -> anyhow::Result<()> {
	game::run().await?;

	Ok(())
}
